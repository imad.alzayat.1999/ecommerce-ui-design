import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';


class LottieAnimationConfig extends StatefulWidget {
  final String file;
  const LottieAnimationConfig({Key? key , required this.file}) : super(key: key);

  @override
  State<LottieAnimationConfig> createState() => _LottieAnimationConfigState();
}

class _LottieAnimationConfigState extends State<LottieAnimationConfig> with TickerProviderStateMixin{
  AnimationController? _controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _controller = AnimationController(vsync: this);
  }
  @override
  Widget build(BuildContext context) {
    return Lottie.asset(
      widget.file,
      controller: _controller,
      fit: BoxFit.contain,
      onLoaded: (composition) {
        _controller!
          ..duration = composition.duration
          ..forward();
      },
    );
  }
}
