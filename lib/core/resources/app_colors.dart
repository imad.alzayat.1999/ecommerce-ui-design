import 'package:flutter/material.dart';

class AppColors{
  static const Color kGhostWhite = Color(0xffF9F9F9);
  static const Color kRaisinBlack = Color(0xff222222);
  static const Color kSpanishGray = Color(0xff9B9B9B);
  static const Color kCharlestonGreen = Color(0xff2D2D2D);
  static const Color kBlack = Colors.black;
  static const Color kWhite = Colors.white;
  static const Color kTransparent = Colors.transparent;
  static const Color kVermilion = Color(0xffDB3022);
}