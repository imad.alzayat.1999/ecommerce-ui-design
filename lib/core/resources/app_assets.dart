class AppImages{

}

class AppIcons{
  static const String mainIconsPath = "assets/icons";

  static const String arrowRightIcon = "$mainIconsPath/arrow_right.svg";
  static const String googleIcon = "$mainIconsPath/google.svg";
  static const String facebookIcon = "$mainIconsPath/facebook.svg";
}

class AppLottie{
  static const String mainLottiePath = "assets/json";

  static const bagJson = "$mainLottiePath/bag.json";
}