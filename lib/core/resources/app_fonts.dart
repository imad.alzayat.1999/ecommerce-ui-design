import 'package:flutter/material.dart';

class AppFontFamilies {
  static const String fontFamily = "Metropolis";
}

class AppFontWeights {
  static const FontWeight regularWeight = FontWeight.w400;
  static const FontWeight mediumWeight = FontWeight.w500;
  static const FontWeight semiBoldWeight = FontWeight.w600;
  static const FontWeight boldWeight = FontWeight.w700;
}

class AppFontSizes {
  static const double f11 = 11;
  static const double f14 = 14;
  static const double f34 = 34;
}

class AppFontStyles {
  static TextStyle getTextStyle({
    required double fontSize,
    required FontWeight fontWeight,
    required Color fontColor,
    required TextDecoration decoration,
  }) {
    return TextStyle(
      fontFamily: AppFontFamilies.fontFamily,
      fontSize: fontSize,
      fontWeight: fontWeight,
      color: fontColor,
      decoration: decoration,
    );
  }

  static TextStyle getRegularTextStyle({
    required double fontSize,
    required Color fontColor,
    TextDecoration decoration = TextDecoration.none,
  }) {
    return getTextStyle(
      fontSize: fontSize,
      fontWeight: AppFontWeights.regularWeight,
      fontColor: fontColor,
      decoration: decoration,
    );
  }

  static TextStyle getMediumTextStyle({
    required double fontSize,
    required Color fontColor,
    TextDecoration decoration = TextDecoration.none,
  }) {
    return getTextStyle(
      fontSize: fontSize,
      fontWeight: AppFontWeights.mediumWeight,
      fontColor: fontColor,
      decoration: decoration,
    );
  }

  static TextStyle getSemiBoldTextStyle({
    required double fontSize,
    required Color fontColor,
    TextDecoration decoration = TextDecoration.none,
  }) {
    return getTextStyle(
      fontSize: fontSize,
      fontWeight: AppFontWeights.semiBoldWeight,
      fontColor: fontColor,
      decoration: decoration,
    );
  }


  static TextStyle getBoldTextStyle({
    required double fontSize,
    required Color fontColor,
    TextDecoration decoration = TextDecoration.none,
  }) {
    return getTextStyle(
      fontSize: fontSize,
      fontWeight: AppFontWeights.boldWeight,
      fontColor: fontColor,
      decoration: decoration,
    );
  }
}
