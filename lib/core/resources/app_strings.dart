class AppStrings{

  /// login string
  static const String loginTitleString = "Login";
  static const String emailString = "Email";
  static const String passwordString = "Password";
  static const String forgotYourPasswordString = "Forgot your password?";
  static const String loginWithSocialMediaString = "Or login with social account";
}