import 'package:flutter/material.dart';
import 'package:ui_design2/core/resources/app_colors.dart';
import 'package:ui_design2/core/resources/app_fonts.dart';

class TextFieldWidget extends StatefulWidget {
  final TextEditingController textEditingController;
  final String labelText;
  final TextInputType textInputType;
  bool obsText;
  final bool isPassword;

  TextFieldWidget({
    Key? key,
    required this.textEditingController,
    required this.labelText,
    required this.textInputType,
    this.obsText = false,
    this.isPassword = false,
  }) : super(key: key);

  @override
  State<TextFieldWidget> createState() => _TextFieldWidgetState();
}

class _TextFieldWidgetState extends State<TextFieldWidget> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 64,
      child: TextFormField(
        controller: widget.textEditingController,
        obscureText: widget.obsText,
        keyboardType: widget.textInputType,
        style: AppFontStyles.getRegularTextStyle(
          fontSize: AppFontSizes.f14,
          fontColor: AppColors.kCharlestonGreen,
        ),
        decoration: InputDecoration(
          labelText: widget.labelText,
          labelStyle: AppFontStyles.getRegularTextStyle(
            fontSize: AppFontSizes.f11,
            fontColor: AppColors.kSpanishGray,
          ),
          suffixIcon: widget.isPassword ? IconButton(
            icon: Icon(
              // Based on passwordVisible state choose the icon
              widget.obsText
                  ? Icons.visibility
                  : Icons.visibility_off,
              color: AppColors.kRaisinBlack,
            ),
            onPressed: () {
              setState(() {
                widget.obsText = !widget.obsText;
              });
            },
          ) : SizedBox.shrink(),
          filled: true,
          contentPadding: EdgeInsets.symmetric(horizontal: 16 , vertical: 0),
          border: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.circular(4),
          ),
          fillColor: AppColors.kWhite,
        ),
      ),
    );
  }
}
