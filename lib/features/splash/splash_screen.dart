import 'package:flutter/material.dart';
import 'package:ui_design2/core/config/animation_config/lottie_animation_config.dart';
import 'package:ui_design2/core/resources/app_assets.dart';
import 'package:ui_design2/core/resources/app_colors.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.kGhostWhite,
      body: Center(
        child: SizedBox(
          width: 500,
          height: 500,
          child: LottieAnimationConfig(
            file: AppLottie.bagJson,
          ),
        ),
      ),
    );
  }
}
