import 'package:flutter/material.dart';
import 'package:ui_design2/core/resources/app_assets.dart';
import 'package:ui_design2/core/resources/app_colors.dart';
import 'package:ui_design2/core/resources/app_fonts.dart';
import 'package:ui_design2/core/resources/app_strings.dart';
import 'package:ui_design2/core/widgets/text_field_widget.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:ui_design2/features/login/components/social_media_component.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  var _emailController = TextEditingController();
  var _passController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.kGhostWhite,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 14),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: Text(
                AppStrings.loginTitleString,
                style: AppFontStyles.getBoldTextStyle(
                  fontSize: AppFontSizes.f34,
                  fontColor: AppColors.kRaisinBlack,
                ),
              ),
            ),
            SizedBox(height: 73),
            TextFieldWidget(
              textEditingController: _emailController,
              labelText: AppStrings.emailString,
              textInputType: TextInputType.emailAddress,
            ),
            SizedBox(height: 8),
            TextFieldWidget(
              textEditingController: _passController,
              labelText: AppStrings.passwordString,
              textInputType: TextInputType.visiblePassword,
              isPassword: true,
              obsText: true,
            ),
            SizedBox(height: 16),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(
                  AppStrings.forgotYourPasswordString,
                  style: AppFontStyles.getRegularTextStyle(
                    fontSize: AppFontSizes.f14,
                    fontColor: AppColors.kRaisinBlack,
                  ),
                ),
                SizedBox(width: 3),
                SizedBox(
                  height: 24,
                  width: 24,
                  child: SvgPicture.asset(
                    AppIcons.arrowRightIcon,
                    color: AppColors.kVermilion,
                  ),
                )
              ],
            ),
            SizedBox(height: 32),
            SizedBox(
              width: double.infinity,
              height: 48,
              child: ElevatedButton(
                onPressed: () {},
                style: ElevatedButton.styleFrom(
                    backgroundColor: AppColors.kVermilion,
                    padding: EdgeInsets.zero,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25),
                    )),
                child: Text(
                  AppStrings.loginTitleString.toUpperCase(),
                  style: AppFontStyles.getRegularTextStyle(
                    fontSize: AppFontSizes.f14,
                    fontColor: AppColors.kWhite,
                  ),
                ),
              ),
            ),
            SizedBox(height: 194),
            Text(
              AppStrings.loginWithSocialMediaString,
              style: AppFontStyles.getRegularTextStyle(
                fontSize: AppFontSizes.f14,
                fontColor: AppColors.kRaisinBlack,
              ),
            ),
            SizedBox(height: 12),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SocialMediaComponent(socialMediaImage: AppIcons.googleIcon),
                SizedBox(width: 16),
                SocialMediaComponent(socialMediaImage: AppIcons.facebookIcon),
              ],
            )
          ],
        ),
      ),
    );
  }
}
