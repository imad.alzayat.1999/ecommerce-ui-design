import 'package:flutter/material.dart';
import 'package:ui_design2/core/resources/app_colors.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SocialMediaComponent extends StatelessWidget {
  final String socialMediaImage;
  const SocialMediaComponent({Key? key , required this.socialMediaImage}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 92,
      height: 64,
      decoration: BoxDecoration(
        color: AppColors.kWhite,
        borderRadius: BorderRadius.circular(24)
      ),
      child: Center(
        child: SizedBox(
          width: 24,
          height: 24,
          child: SvgPicture.asset(socialMediaImage),
        ),
      ),
    );
  }
}
